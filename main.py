"""_docs_
install opencv for python
"""
import cv2
import os

images = os.listdir("./images")

for image in images:
    
    np_img = cv2.imread("./images/"+image)
    print(f'image shape: ', {np_img.shape})
